<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('load_form');
});

Route::post('/rs-load', 'LoadController@load');

Route::get(storage_path() . '/app/images/{time}/{filename}', function ($time, $filename) {
   return response()->download(storage_path() . '/app/images/' . $time . '/' . $filename, $filename);
});
