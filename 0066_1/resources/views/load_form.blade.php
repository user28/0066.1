<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Создание креативов</title>
    <style>
        .form-group input, .custom-file, select {
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-6">
            <form action="/rs-load" method="post" enctype="multipart/form-data">
                <div class="form-group"><label for="url">Ссылка:</label><input type="url" name="url" id="url"
                                                                               class="form-control">
                    <label for="price">Ценник:</label><input type="text" name="price" id="price" class="form-control">
                    <label for="camp_id">ID кампании:</label><input type="number" name="camp_id" id="camp_id"
                                                                    class="form-control">
                    <label for="rs">Рекламная кампания:</label><select name="rs" id="rs" class="form-control">
                        <option value="megapush">Megapu.sh</option>
                        <option value="mgid">MGID</option>
                        <option value="daopush">Daopush</option>
                        <option value="datspush">Datspush</option>
                        <option value="pushads">Pushads</option>
                        <option value="richpush">Richpush</option>
                        <option value="clickscloud">Clickscloud</option>
                        <option value="propellerads">Propellerads</option>
                        <option value="advertlink">Advertlink</option>
                    </select>

                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="images" name="images"
                               accept=".zip, .rar" required>
                        <label class="custom-file-label" for="images">Картинки</label>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="text" name="text" accept=".txt" required>
                        <label class="custom-file-label" for="text">Текст</label>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-primary">Загрузить</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</html>