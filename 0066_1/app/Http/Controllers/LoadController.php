<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MGIDService;
use ZipArchive;

class LoadController extends Controller
{
    public function load(Request $request)
    {
        $images_zip = $request->file('images')->store('images');
        $text_file = $request->file('text')->store('texts');

        $url = $request->input('url');
        $price = $request->input('price');
        $camp_id = $request->input('camp_id');
        $rs = $request->input('rs');

        $images = $this->unzip($images_zip);

        $texts = $this->get_texts($text_file);

        $output = [];

        $index = 0;

        foreach ($images as $image) {
            $data['url'] = $url;
            $data['price'] = $price;
            $data['camp_id'] = $camp_id;
            $data['rs'] = $rs;
            $data['image'] = $image;
            $data['text'] = $texts[$index];

            array_push($output, $data);

            $index++;
        }

        $output_json = [];

        foreach ($output as $item) {
            array_push($output, $this->send_to_rs($item));
        }

        return response()->json($output_json);

    }

    private function unzip($path)
    {
        $zip = new ZipArchive();

        $unzip_path = storage_path() . '/app/images/' . time() . '/';

        if ($zip->open(storage_path() . '/app/' . $path) === true) {
            $zip->extractTo($unzip_path);
            $zip->close();
        }

        unlink(storage_path() . '/app/' . $path);

        return glob($unzip_path . '*.jpg');
    }

    private function get_texts($path)
    {
        $texts = file(storage_path() . '/app/' . $path, FILE_IGNORE_NEW_LINES);

        unlink(storage_path() . '/app/' . $path);

        return $texts;
    }

    private function send_to_rs($item)
    {
        $service = false;

        echo $item['rs'];

        switch ($item['rs']) {
            case 'mgid':
                echo 'MGID';
                $service = new MGIDService();
                break;
            default:
                return false;
        }

        return $service->add($item);
    }
}
