<?php

namespace App\Services;

class MGIDService implements \App\Services\IService
{
    const AUTH_URL = 'api.mgid.com/v1/auth/token';
    const API_BASE_URL = 'api.mgid.com/v1/goodhits/clients/';

    private $auth = false;

    public function __construct()
    {
        $this->auth = $this->auth('bonjur235@gmail.com', 'l3QDTGEM');

        echo json_encode($this->auth);
    }

    public function add($item)
    {
        if (!$this->auth) {
            return false;
        }

        $url = self::API_BASE_URL . $this->auth['idAuth'] . '/teasers?token=' . $this->auth['token'];

        $post_params['url'] = $item['url'];
        $post_params['campaignId'] = $item['camp_id'];
        $post_params['title'] = $item['text'];
        $post_params['imageLink'] = url('/') . $item['image'];
        $post_params['category'] = 146;
        $post_params['priceOfClick'] = 0.5;
        $post_params['cropWidth'] = '492px';
        $post_params['cropWidthQuadratic'] = '328px';
        $post_params['cropTopQuadratic'] = 0;
        $post_params['cropLeftQuadratic'] = 0;
        $post_params['whetherShowGoodPrice'] = 1;
        $post_params['goodPrice'] = $item['price'];
        $post_params['currency'] = 5;

        $result = $this->curl($url, http_build_query($post_params), 'POST');

        if (!isset($result['errors'])) {
            return $result;
        }

        return false;
    }

    private function auth($login, $password)
    {
        $post_params['email'] = $login;
        $post_params['password'] = $password;

        $result = $this->curl_post(self::AUTH_URL, http_build_query($post_params));

        if (!isset($result['errors'])) {
            return $result;
        }

        return false;
    }

    function curl_post($base_url, $post_params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    function curl($base_url, $patch_params, $method)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $patch_params);

        $result = curl_exec($ch);
        curl_close($ch);

        echo $result;

        return json_decode($result, true);
    }
}