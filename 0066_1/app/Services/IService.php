<?php


namespace App\Services;


interface IService
{
    function __construct();

    public function add($item);
}